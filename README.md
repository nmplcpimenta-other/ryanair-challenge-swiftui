# Ryanair Challenge SwiftUI

## Summary

This project's goal is to comply to a mobile challenge's requirements while exploring the SwiftUI framework.

### Context

This project uses Ryanair API to load airport stations and search for flights.

## Install

Start by cloning the project.

### Prerequisites

1. [Cocoapods](https://guides.cocoapods.org/using/getting-started.html)
	+ If you do not have it installed you can execute the following command on your terminal.

	```
	sudo gem install cocoapods
	```
	+ After installing Cocoapods, you should install the Pods themselves.

	```
	pod install
	```
	**NOTE**: This command should be run from within `project/RyanairChallenge/` directory (where `Podfile` is located).

## Project

### Design Pattern

I used a simplified approximation of Viper, but without protocols for request/response and without scene construction.

### Project Structure

```
+-- Resources
+-- Sources
|   +-- UI
|   +-- Presentation
|   +-- Core
|   +-- Repositories
```

1. **Resources**: Location for all resources of the project like images, animations and storyboards.
2. **Sources**:
	+ **UI**: Location for all view files.
	+ **Presentation**: Location for all presenters and view models.
	+ **Core**: Location for all interactors and models.
	+ **Repositories**: Location for all repositories and their models.

### Frameworks Used
1. **SwiftUI**: New layout framework which uses Swift code to describe the UI. I used it because I wanted to learn more about it and the challenge didn't require that I used storyboards for the UI.
2. **Moya/Reactive Swift**: Abstraction for API and network communication. I used it to represent the Ryanair API and communicate with it while treating the response using Reactive Swift.
3. **Lottie**: Animation framework that uses animations described with JSON. I used it to show the loading animation.