//
//  RyanairRepository.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 05/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import Foundation
import Moya

class RyanairRepository {
    
    private var provider: MoyaProvider<RyanairService>
    
    init() {
        provider = MoyaProvider<RyanairService>(plugins: [NetworkLoggerPlugin()])
    }
    
    func getStations(successCallback: @escaping (([StationRyanairModel]) -> Void),
                     errorCallback: @escaping ((String) -> Void)) {
        
        provider.reactive.request(.getStations).start { event in
            
            switch event {
            case let .value(moyaResponse):
                
                do {
                    try _ = moyaResponse.filterSuccessfulStatusCodes()
                    
                    do {
                        let stationsResult = try moyaResponse.map(StationsRyanairResponse.self).stations
                        
                        successCallback(stationsResult)
                    } catch let error {
                        debugPrint(error)
                        errorCallback(error.localizedDescription)
                    }

                } catch let error {
                    debugPrint(error)
                    errorCallback(error.localizedDescription)
                }
            case let .failed(error):
                errorCallback(error.localizedDescription)
            default:
                break
            }
        }
    }
    
    func getTrips(searchModel: SearchRyanairModel,
                  successCallback: @escaping ((TripsRyanairResponse) -> Void),
                  errorCallback: @escaping ((String) -> Void)) {
        
        provider.reactive.request(.getTrips(
            originCode: searchModel.originCode,
            destinationCode: searchModel.destinationCode,
            departureDate: searchModel.departureDate,
            adultCount: searchModel.adultCount,
            teenCount: searchModel.teenCount,
            childrenCount: searchModel.childrenCount)).start { event in
                
                switch event {
                case let .value(moyaResponse):
                    
                    do {
                        try _ = moyaResponse.filterSuccessfulStatusCodes()
                        
                        do {
                            let tripsResult = try moyaResponse.map(TripsRyanairResponse.self)
                            
                            successCallback(tripsResult)
                        } catch let error {
                            debugPrint(error)
                            errorCallback(error.localizedDescription)
                        }

                    } catch let error {
                        debugPrint(error)
                        errorCallback(error.localizedDescription)
                    }
                case let .failed(error):
                    errorCallback(error.localizedDescription)
                default:
                    break
                }
        }
    }
}
