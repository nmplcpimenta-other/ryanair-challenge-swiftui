//
//  RyanairService.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 05/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import Moya

enum RyanairService {
    
    case getStations
    case getTrips(originCode: String, destinationCode: String, departureDate: String, adultCount: Int, teenCount: Int, childrenCount: Int)
}

extension RyanairService: TargetType {
    
    var baseURL: URL {
        switch self {
        case .getStations:
            return URL(string: "https://tripstest.ryanair.com/static/stations.json")!
        case .getTrips:
            return URL(string: "https://sit-nativeapps.ryanair.com/api/v4/Availability")!
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        
        switch self {
        case .getStations:
            
            return .requestPlain
        case .getTrips(let originCode, let destinationCode, let departureDate, let adultCount, let teenCount, let childrenCount):
            
            var params: [String: Any] = [:]
            
            params["origin"] = originCode
            params["destination"] = destinationCode
            params["dateout"] = departureDate
            params["datein"] = ""
            params["flexdaysbeforeout"] = 0
            params["flexdaysout"] = 6
            params["flexdaysbeforein"] = 0
            params["flexdaysin"] = 0
            params["adt"] = adultCount
            params["teen"] = teenCount
            params["chd"] = childrenCount
            params["roundtrip"] = "false"
            params["ToUs"] = "AGREED"
            
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}
