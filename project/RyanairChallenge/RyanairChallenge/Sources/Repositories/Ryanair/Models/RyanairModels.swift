//
//  RyanairModels.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 05/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import Foundation

struct StationsRyanairResponse: Codable {
    
    var stations: [StationRyanairModel]
}

struct StationRyanairModel: Codable {
    
    var name: String
    var code: String
    
    func toModel() -> StationModel {
        return StationModel(order: 0, name: name, codename: code)
    }
    
    func toModel(order: Int) -> StationModel {
        return StationModel(order: order, name: name, codename: code)
    }
}

struct SearchRyanairModel {
    
    var originCode: String
    var destinationCode: String
    var departureDate: String
    var adultCount: Int
    var teenCount: Int
    var childrenCount: Int
    
    static func fromModel(model: SearchModel) -> SearchRyanairModel {
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = Configs.dfSearchRyanairModelDate
        
        return SearchRyanairModel(
            originCode: model.origin.codename,
            destinationCode: model.destination.codename,
            departureDate: dateFormatter.string(from: model.departureDate),
            adultCount: model.adultCount,
            teenCount: model.teenCount,
            childrenCount: model.childrenCount)
    }
}

struct TripsRyanairResponse: Codable {
    
    var currency: String
    var trips: [TripRyanairModel]
}

struct TripRyanairModel: Codable {
    
    var dates: [TripDateRyanairModel]
}

struct TripDateRyanairModel: Codable {
    
    var dateOut: String
    var flights: [FlightRyanairModel]
}

struct FlightRyanairModel: Codable {
    
    var time: [String]
    var regularFare: RegularFareRyanairModel
    var flightNumber: String
}

struct RegularFareRyanairModel: Codable {
    
    var fares: [FareRyanairModel]
}

struct FareRyanairModel: Codable {
    
    var amount: Float
    var type: String
}
