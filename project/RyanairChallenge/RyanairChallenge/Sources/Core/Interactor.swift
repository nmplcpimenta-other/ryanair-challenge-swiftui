//
//  Interactor.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 05/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import Foundation

class Interactor {
    
    let ryanairRepository: RyanairRepository
    
    init() {
        ryanairRepository = RyanairRepository()
    }
    
    func getStations(successCallback: @escaping (([StationModel]) -> Void),
                     errorCallback: @escaping ((String) -> Void)) {
        
        ryanairRepository.getStations(
            successCallback: { ryanairStations in
                let stations: [StationModel] = ryanairStations.enumerated().map { (index, ryanairStation) in
                    ryanairStation.toModel(order: index)
                }
                
                successCallback(stations)
        }, errorCallback: { errorMessage in
            errorCallback(errorMessage)
        })
    }
    
    func searchTrips(searchModel: SearchModel,
                     successCallback: @escaping ((TripsModel) -> Void),
                     errorCallback: @escaping ((String) -> Void)) {
        
        let searchRyanairModel: SearchRyanairModel = SearchRyanairModel.fromModel(model: searchModel)
        
        ryanairRepository.getTrips(
            searchModel: searchRyanairModel,
            successCallback: { tripsResponse in
                
                let tripsModel: TripsModel = self.dealWithTripsResponse(tripsResponse)
                
                successCallback(tripsModel)
        }, errorCallback: { errorMessage in
            
            errorCallback(errorMessage)
        })
    }
}

extension Interactor {
    
    private func dealWithTripsResponse(_ tripsResponse: TripsRyanairResponse) -> TripsModel {
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = Configs.dfTripResponse
        
        var tripsModel: TripsModel = TripsModel(
            currency: Currency(value: tripsResponse.currency),
            trips: [])
        
        for trip in tripsResponse.trips {
            
            var tripModel: TripModel = TripModel(dates: [])
            
            for tripDate in trip.dates {
                
                if let dateOut: Date = dateFormatter.date(from: tripDate.dateOut) {
                    
                    var tripDateModel: TripDateModel = TripDateModel(
                        dateOut: dateOut,
                        flights: [])
                    
                    for flight in tripDate.flights {
                        
                        if let timeOut: Date = dateFormatter.date(from: flight.time[0]) {
                            
                            var flightModel: FlightModel = FlightModel(
                                datetimeOut: timeOut,
                                flightNumber: flight.flightNumber,
                                adultRegularFare: 0,
                                teenRegularFare: 0,
                                childRegularFare: 0)
                            
                            for fare in flight.regularFare.fares {
                                
                                if fare.type.uppercased() == Configs.fareTypeAdult {
                                    flightModel.adultRegularFare = fare.amount
                                } else if fare.type.uppercased() == Configs.fareTypeTeen {
                                    flightModel.teenRegularFare = fare.amount
                                } else {
                                    flightModel.childRegularFare = fare.amount
                                }
                            }
                            
                            tripDateModel.flights.append(flightModel)
                        }
                    }
                    
                    tripModel.dates.append(tripDateModel)
                }
            }
            
            tripsModel.trips.append(tripModel)
        }
        
        return tripsModel
    }
}
