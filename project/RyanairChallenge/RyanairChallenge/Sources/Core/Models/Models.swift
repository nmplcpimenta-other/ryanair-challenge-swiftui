//
//  Models.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 04/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import Foundation

struct SearchModel {
    
    var origin: StationModel
    var destination: StationModel
    var departureDate: Date
    var adultCount: Int
    var teenCount: Int
    var childrenCount: Int
}

struct StationModel {
    
    var order: Int
    var name: String
    var codename: String
}

struct TripsModel {
    
    var currency: Currency
    var trips: [TripModel]
}

struct TripModel {
    
    var dates: [TripDateModel]
}

struct TripDateModel {
    
    var dateOut: Date
    var flights: [FlightModel]
}

struct FlightModel {
    
    var datetimeOut: Date
    var flightNumber: String
    var adultRegularFare: Float
    var teenRegularFare: Float
    var childRegularFare: Float
}

enum Currency: String {
    
    case EUR = "EUR"
    
    var symbol: String {
        switch self {
        case .EUR:
            return "€"
        }
    }
    
    init(value: String) {
        self = Currency(rawValue: value) ?? Currency.EUR
    }
}
