//
//  Configs.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 06/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

internal enum Configs {
    
    internal static let dfTripResponse: String = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    internal static let dfTripViewModelDate: String = "dd-MM-yyyy"
    internal static let dfFlightViewModelDate: String = "HH:mm"
    internal static let dfSearchRyanairModelDate: String = "yyyy-MM-dd"
    
    internal static let fareTypeAdult: String = "ADT"
    internal static let fareTypeTeen: String = "TEEN"
    
    internal static let lottieLoading: String = "loading"
}
