//
//  UIApplication+Extensions.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 04/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import UIKit

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
