//
//  SearchFormView.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 04/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import SwiftUI

struct SearchFormView: View {
    
    // State variable used to control navigation endpoint within this view
    @State private var navigationTag: String? = nil
    
    let presenter: SearchFormPresenter
    
    @ObservedObject var stationsVM: StationsViewModel
    
    // Auxiliary property to help pass along result view model from search result callback
    // to navigation link next screen
    @State private var resultVM: SearchResultViewModel = SearchResultViewModel()
    
    @State private var isShowingAlert: Bool = false
    @State private var alertVM: AlertViewModel?
    
    // Form fields' state properties
    @State private var selectedDate: Date = Date()
    @State private var selectedOriginIndex: Int = 0
    @State private var selectedDestinationIndex: Int = 0
    @State private var adultCount: Int = 1
    @State private var teenCount: Int = 0
    @State private var childrenCount: Int = 0
    
    @State private var bottomPadding: CGFloat = 0.0
    
    @State private var isLoadingTrips: Bool = false
    @State private var isLoadingStations: Bool = false
    
    var body: some View {
        ZStack {
            NavigationView {
                VStack(spacing: 0) {
                    NavigationLink(destination: TripListView(resultVM: self.resultVM),
                                   tag: NavigationTags.tripList,
                                   selection: self.$navigationTag) { EmptyView() }
                    
                    Form {
                        Section(header: Text(Strings.SearchFormView.sectionStations)) {
                            SearchFormStationFieldView(
                                isLoadingStations: $isLoadingStations,
                                selectedIndex: $selectedOriginIndex,
                                stationsVM: stationsVM,
                                label: Strings.SearchFormView.fieldOrigin)

                            SearchFormStationFieldView(
                                isLoadingStations: $isLoadingStations,
                                selectedIndex: $selectedDestinationIndex,
                                stationsVM: stationsVM,
                                label: Strings.SearchFormView.fieldDestination)
                        }
                        
                        Section(header: Text(Strings.SearchFormView.sectionDates)) {
                            DatePicker(Strings.SearchFormView.fieldDepartureDate,
                                       selection: self.$selectedDate,
                                       in: Date()...,
                                       displayedComponents: .date)
                        }
                        
                        Section(header: Text(Strings.SearchFormView.sectionNumberOfTickets)) {
                            Stepper(value: self.$adultCount,
                                    in: 1...6,
                                    label: {
                                Text("\(Strings.SearchFormView.fieldAdults)\(self.adultCount)")
                            })
                            
                            Stepper(value: self.$teenCount,
                                    in: 0...6,
                                    label: {
                                Text("\(Strings.SearchFormView.fieldTeens)\(self.teenCount)")
                            })
                            
                            Stepper(value: self.$childrenCount,
                                    in: 0...6,
                                    label: {
                                Text("\(Strings.SearchFormView.fieldChildren)\(self.childrenCount)")
                            })
                        }
                        
                        Button(action: {
                            
                            self.didTapButtonSearch()
                            
                        }) {
                            Text(Strings.SearchFormView.buttonSearch)
                        }
                        .disabled(self.stationsVM.stations.count == 0)
                    }
                    .navigationBarTitle(Strings.SearchFormView.navigationBarTitle)
                }
                .padding([.bottom], self.bottomPadding)
                .onAppear(perform: doOnAppear)
                .onDisappear(perform: doOnDisappear)
                .alert(isPresented: self.$isShowingAlert) {
                    Alert(title: Text(self.alertVM?.title ?? ""),
                          message: Text(self.alertVM?.message ?? ""),
                          dismissButton: .default(Text(self.alertVM?.dismissButtonText ?? "")))
                }
            }
            
            if self.isLoadingTrips {
                VStack {
                    Spacer()
                    HStack {
                        Spacer()
                        LottieView(name: Configs.lottieLoading,
                                   play: .constant(1),
                                   isLooping: true)
                            .frame(width:200, height:200)
                        Spacer()
                    }
                    Spacer()
                }
                .background(Color.black.opacity(0.3))
            }
        }
    }
    
    init() {
        
        self.presenter = SearchFormPresenter()
        
        self.stationsVM = presenter.stationsVM
    }
    
    private func doOnAppear() {
        
        addKeyboardObservers()
        
        self.isLoadingStations = true
        
        self.presenter.getStations(successCallback: {
            
            self.isLoadingStations = false
        }, errorCallback: { errorMessage in
            
            self.alertVM = AlertViewModel(
                title: Strings.alertTitle,
                message: errorMessage,
                dismissButtonText: Strings.alertDismissButton)
            
            self.isShowingAlert = true
            
            self.isLoadingStations = false
        })
    }
    
    private func doOnDisappear() {
        
        removeKeyboardObservers()
    }
    
    private func addKeyboardObservers() {
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main) { key in
            let value = key.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            self.bottomPadding = value.height
        }

        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: .main) { key in
            self.bottomPadding = 0.0
        }
    }
    
    private func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(UIResponder.keyboardWillShowNotification)
        NotificationCenter.default.removeObserver(UIResponder.keyboardWillHideNotification)
    }
    
    private func didTapButtonSearch() {
        
        let searchVM = SearchViewModel(
            originIndex: self.selectedOriginIndex,
            destinationIndex: self.selectedDestinationIndex,
            departureDate: self.selectedDate,
            adultCount: self.adultCount,
            teenCount: self.teenCount,
            childrenCount: self.childrenCount)
        
        self.isLoadingTrips = true

        self.presenter.searchTrips(
            searchVM: searchVM,
            successCallback: { result in

                // Update result view model to propagate to navigation link
                self.resultVM = result

                // Trigger navigation link
                self.navigationTag = NavigationTags.tripList
                
                self.isLoadingTrips = false
        }, errorCallback: { errorMessage in
            self.alertVM = AlertViewModel(
                title: Strings.alertTitle,
                message: errorMessage,
                dismissButtonText: Strings.alertDismissButton)
            
            self.isShowingAlert = true
            
            self.isLoadingTrips = false
        })
    }
}

struct SearchFormView_Previews: PreviewProvider {
    static var previews: some View {
        SearchFormView()
    }
}
