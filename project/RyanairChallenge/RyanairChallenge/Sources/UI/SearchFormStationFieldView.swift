//
//  SearchFormStationFieldView.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 05/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import SwiftUI

struct SearchFormStationFieldView: View {
    
    @Binding var isLoadingStations: Bool
    @Binding var selectedIndex: Int
    @ObservedObject var stationsVM: StationsViewModel
    var label: String
    
    var body: some View {
        NavigationLink(destination: StationListView(selectedIndex: $selectedIndex, stationsVM: stationsVM)) {
            
            HStack {
                Text(label)
                Spacer()
                if !self.isLoadingStations {
                    Text(self.stationsVM.stations.count != 0 ?
                        self.stationsVM.stations[selectedIndex].name + " (" + self.stationsVM.stations[selectedIndex].codename + ")" :
                        "")
                } else {
                    LottieView(name: Configs.lottieLoading,
                               play: self.isLoadingStations ? .constant(1) : .constant(0),
                               isLooping: true)
                        .frame(width:30, height:30)
                }
            }
        }.disabled(self.stationsVM.stations.count == 0)
    }
}

struct SearchFormStationFieldPreviewHelper: View {
    
    @State private var isLoadingStations: Bool = false
    @State private var selectedIndex: Int = 0
    @ObservedObject private var stationsVM: StationsViewModel = StationsViewModel(stations: [
        StationViewModel(id: 0, order: 0, name: "Lisbon", codename: "LIS")
    ])
    private var label: String = "Origin"
    
    var body: some View {
        SearchFormStationFieldView(isLoadingStations: $isLoadingStations, selectedIndex: $selectedIndex, stationsVM: stationsVM, label: label)
    }
}

struct SearchFormStationFieldView_Previews: PreviewProvider {
    static var previews: some View {
        SearchFormStationFieldPreviewHelper()
        .previewLayout(PreviewLayout.sizeThatFits)
        .previewDisplayName("Default preview")
    }
}
