//
//  StationListView.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 05/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import SwiftUI

struct StationListView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @Binding var selectedIndex: Int
    @ObservedObject var stationsVM: StationsViewModel
    
    @State private var searchText: String = ""
    
    var body: some View {
        VStack(spacing: 0) {
            VStack(spacing: 0) {
                SearchBarView(text: $searchText).padding([.vertical], 10)
                
                Color.secondary.frame(height: 2)
            }
            
            List {
                ForEach(self.stationsVM.stations.filter { stationVM in
                    self.searchText.isEmpty ?
                        true :
                        (stationVM.name.lowercased()
                            .contains(self.searchText.lowercased()) ||
                        stationVM.codename.lowercased()
                            .contains(self.searchText.lowercased()))
                }) { stationVM in
                    HStack(spacing: 0) {
                        Text(stationVM.name)
                        Spacer()
                        Text("(\(stationVM.codename))")
                        
                        ZStack {
                            if self.selectedIndex == stationVM.order {
                                Image(systemName: "checkmark")
                                    .foregroundColor(.blue)
                                    .font(Font.system(size: 24).bold())
                            } else {
                                EmptyView()
                            }
                        }.frame(width: 30)
                    }
                    .contentShape(Rectangle())
                    .onTapGesture {
                        self.selectedIndex = stationVM.order
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }
            }
        }.navigationBarTitle("", displayMode: .inline)
    }
}

struct StationListViewPreviewHelper: View {
    
    @State private var selectedIndex: Int = 0
    @ObservedObject private var stationsVM: StationsViewModel = StationsViewModel(
        stations: [
            StationViewModel(
                id: 0, order: 0,
                name: "Bla", codename: "ABC"),
            StationViewModel(
                id: 1, order: 1,
                name: "Ble", codename: "BCD"),
            StationViewModel(
                id: 2, order: 2,
                name: "Bli", codename: "CDE"),
            StationViewModel(
                id: 3, order: 3,
                name: "Blo", codename: "DEF")
        ])

    var body: some View {
         StationListView(selectedIndex: $selectedIndex,
                         stationsVM: stationsVM)
    }
}

struct StationListView_Previews: PreviewProvider {

    @State private var selectedIndex: Int = 0

    static var previews: some View {
        StationListViewPreviewHelper()
    }
}
