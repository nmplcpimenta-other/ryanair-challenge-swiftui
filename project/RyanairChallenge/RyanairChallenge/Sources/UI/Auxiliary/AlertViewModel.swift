//
//  AlertViewModel.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 06/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

struct AlertViewModel {
    
    var title: String
    var message: String
    var dismissButtonText: String
}
