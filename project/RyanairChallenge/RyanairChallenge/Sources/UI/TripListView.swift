//
//  TripListView.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 01/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import SwiftUI

struct TripListView: View {
    
    let resultVM: SearchResultViewModel
    
    var body: some View {
        VStack(spacing: 0) {
            
            TripListSummaryView(resultVM: resultVM)
            
            VStack {
                if self.resultVM.tripsByDate.count != 0 {
                    List {
                        ForEach(self.resultVM.tripsByDate) { tripsDate in
                            Section(header: Text(tripsDate.date), footer: EmptyView().frame(height: 1)) {
                                ForEach(tripsDate.flights) { flight in
                                    TripListRowView(flight: flight)
                                }
                            }
                        }
                        
                    }
                    .padding([.top], 0)
                    .listStyle(GroupedListStyle())
                } else {
                    Spacer()
                    HStack {
                        Spacer()
                        Text(Strings.TripListView.noFlightsFound)
                        Spacer()
                    }
                    Spacer()
                }
            }
        }.navigationBarTitle("", displayMode: .inline)
    }
    
    init(resultVM: SearchResultViewModel) {
        UITableView.appearance().tableFooterView = UIView()
        UITableViewCell.appearance().selectionStyle = .none
        
        self.resultVM = resultVM
    }
}

struct TripListView_Previews: PreviewProvider {
    static var previews: some View {
        TripListView(resultVM: SearchResultViewModel(
            originName: "Lisbon",
            originCodename: "LIS",
            destinationName: "London",
            destinationCodename: "LON",
            departureDate: Date(),
            adultCount: 4,
            teenCount: 10,
            childrenCount: 0,
            tripsByDate: [
                TripDateViewModel(id: 0, date: "2020-06-07", flights: [
                    FlightViewModel(id: 0, time: "14:00", flightNumber: "FJG 3414", regularFare: "101.43 €"),
                    FlightViewModel(id: 1, time: "15:00", flightNumber: "FJG 3414", regularFare: "101.43 €")
                ]),
                TripDateViewModel(id: 1, date: "2020-06-08", flights: [
                    FlightViewModel(id: 2, time: "09:30", flightNumber: "FJG 3414", regularFare: "101.43 €"),
                    FlightViewModel(id: 3, time: "11:00", flightNumber: "FJG 3414", regularFare: "101.43 €"),
                    FlightViewModel(id: 13, time: "12:30", flightNumber: "FJG 3414", regularFare: "101.43 €")
                ]),
                TripDateViewModel(id: 2, date: "2020-06-09", flights: [
                    FlightViewModel(id: 4, time: "10:00", flightNumber: "FJG 3414", regularFare: "101.43 €")
                ]),
                TripDateViewModel(id: 3, date: "2020-06-10", flights: [
                    FlightViewModel(id: 6, time: "08:15", flightNumber: "FJG 3414", regularFare: "101.43 €"),
                    FlightViewModel(id: 7, time: "10:00", flightNumber: "FJG 3414", regularFare: "101.43 €"),
                    FlightViewModel(id: 8, time: "16:15", flightNumber: "FJG 3414", regularFare: "101.43 €"),
                    FlightViewModel(id: 9, time: "20:00", flightNumber: "FJG 3414", regularFare: "101.43 €")
                ])
            ]
        ))
    }
}
