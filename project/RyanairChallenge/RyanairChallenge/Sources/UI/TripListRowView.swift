//
//  TripListRowView.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 02/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import SwiftUI

struct TripListRowView: View {
    
    var flight: FlightViewModel
    
    var body: some View {
        HStack(alignment: .center, spacing: 0) {
            HStack(spacing: 6) {
                Image(systemName: "clock")
                    .font(.headline)
                Text(flight.time)
                    .font(.subheadline)
                Spacer()
            }.frame(width: 80)
            
            HStack(spacing: 6) {
                Image(systemName: "airplane")
                Text(flight.flightNumber)
                    .font(.callout)
            }
            
            Spacer()
            
            VStack(spacing: 0) {
                if flight.regularFare.isEmpty {
                    Text("---")
                } else {
                    Text(flight.regularFare).font(.subheadline)
                }
            }
            
        }
    }
}

struct TripListRowView_Previews: PreviewProvider {
    static var previews: some View {
        TripListRowView(
            flight: FlightViewModel(
                id: 0,
                time: "##:##",
                flightNumber: "FR 7329",
                regularFare: "105.99 " + Currency.EUR.symbol))
    }
}
