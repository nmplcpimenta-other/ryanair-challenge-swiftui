//
//  TripListSummaryView.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 04/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import SwiftUI

struct TripListSummaryView: View {
    
    let resultVM: SearchResultViewModel
    let dateFormatter: DateFormatter
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            VStack(alignment: .leading, spacing: 6) {
                HStack(spacing: 4) {
                    Text(Strings.TripListSummaryView.labelTrip).foregroundColor(Color.secondary)
                    Spacer()
                    Text("\(resultVM.originName) (\(resultVM.originCodename))" + "->" +
                         "\(resultVM.destinationName) (\(resultVM.destinationCodename))")
                        .foregroundColor(Color.primary)
                        .font(.subheadline)
                        
                }
                HStack {
                    Text(Strings.TripListSummaryView.labelDate).foregroundColor(Color.secondary)
                    Spacer()
                    Text(dateFormatter.string(from: resultVM.departureDate))
                        .foregroundColor(Color.primary)
                        .font(.subheadline)
                }
                HStack {
                    Text(Strings.TripListSummaryView.labelTickets).foregroundColor(Color.secondary)
                    Spacer()
                    Text(Strings.TripListSummaryView.ticketCounts(
                            resultVM.adultCount,
                            resultVM.teenCount,
                            resultVM.childrenCount))
                        .foregroundColor(Color.primary)
                        .font(.subheadline)
                }
            }
            .padding([.vertical], 15)
            .padding([.horizontal], 15)
            
            Color.secondary.frame(height: 2)
        }
    }
    
    init(resultVM: SearchResultViewModel) {
        self.resultVM = resultVM
        
        self.dateFormatter = DateFormatter()
        self.dateFormatter.dateFormat = Configs.dfTripViewModelDate
    }
}

struct TripListSummaryView_Previews: PreviewProvider {
    static var previews: some View {
        TripListSummaryView(resultVM: SearchResultViewModel(
            originName: "Lisboa",
            originCodename: "LIS",
            destinationName: "Londres",
            destinationCodename: "LON",
            departureDate: Date(),
            adultCount: 4,
            teenCount: 10,
            childrenCount: 0,
            tripsByDate: []))
        .previewLayout(PreviewLayout.sizeThatFits)
        .previewDisplayName("Default preview")
    }
}
