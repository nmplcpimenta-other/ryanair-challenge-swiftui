//
//  SearchFormPresenter.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 04/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import SwiftUI

class SearchFormPresenter {
    
    private var interactor: Interactor
    
    @ObservedObject var stationsVM: StationsViewModel
    private var stations: [StationModel] = []
    
    init() {
        
        self.interactor = Interactor()
        
        self.stationsVM = StationsViewModel()
        
        stationsVM.stations = []
    }
    
    func getStations(successCallback: @escaping (() -> Void), errorCallback: @escaping ((String) -> Void)) {
        
        interactor.getStations(
            successCallback: { stations in
                
                // Backup stations list
                self.stations = stations
                
                // Transform stations into stations view models
                let tempStationsVM: [StationViewModel] = stations.map { station in
                    StationViewModel.fromModel(model: station)
                }

                DispatchQueue.main.async {
                    // Update stations VM to trigger UI update
                    self.stationsVM.stations = tempStationsVM
                    
                    successCallback()
                }
        }, errorCallback: { errorMessage in
            
            DispatchQueue.main.async {
                errorCallback(errorMessage)
            }
        })
    }
    
    func searchTrips(searchVM: SearchViewModel,
                     successCallback: @escaping ((SearchResultViewModel) -> Void),
                     errorCallback: @escaping ((String) -> Void)) {
        
        interactor.searchTrips(
            searchModel: searchVM.toModel(stations: stations),
            successCallback: { tripsModel in
                
                // Convert trips model into view models and create output result object (to send to next screen)
                let searchResult: SearchResultViewModel = self.dealWithTripsModel(searchVM, tripsModel)
                
                DispatchQueue.main.async {
                    successCallback(searchResult)
                }
        }, errorCallback: { errorMessage in
            
            DispatchQueue.main.async {
                errorCallback(errorMessage)
            }
        })
    }
}

extension SearchFormPresenter {
    
    private func dealWithTripsModel(_ searchVM: SearchViewModel, _ tripsModel: TripsModel) -> SearchResultViewModel {
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = Configs.dfTripViewModelDate
        
        let dateFormatterTime: DateFormatter = DateFormatter()
        dateFormatterTime.dateFormat = Configs.dfFlightViewModelDate
        
        var tripsByDate: [TripDateViewModel] = []
        
        for trip in tripsModel.trips {
            
            for date in trip.dates {
                
                var tripDate: TripDateViewModel = TripDateViewModel(
                    id: date.dateOut.description.hashValue,
                    date: dateFormatter.string(from: date.dateOut),
                    flights: [])
                
                for flight in date.flights {
                    
                    // Regular fare is a result of each fare type amount times type ticket count
                    let regularFare: Float = Float(searchVM.adultCount) * flight.adultRegularFare +
                        Float(searchVM.teenCount) * flight.teenRegularFare +
                        Float(searchVM.childrenCount) * flight.childRegularFare
                    
                    let regularFareText: String = String(format: "%.2f %@", regularFare, tripsModel.currency.symbol)
                    
                    let flightVM: FlightViewModel = FlightViewModel(
                        id: flight.datetimeOut.description.hashValue,
                        time: dateFormatterTime.string(from: flight.datetimeOut),
                        flightNumber: flight.flightNumber,
                        regularFare: regularFareText)
                    
                    tripDate.flights.append(flightVM)
                }
                
                if tripDate.flights.count > 0 {
                    tripsByDate.append(tripDate)
                }
            }
        }
        
        let result: SearchResultViewModel = SearchResultViewModel(
            originName: self.stationsVM.stations[searchVM.originIndex].name,
            originCodename: self.stationsVM.stations[searchVM.originIndex].codename,
            destinationName: self.stationsVM.stations[searchVM.destinationIndex].name,
            destinationCodename: self.stationsVM.stations[searchVM.destinationIndex].codename,
            departureDate: searchVM.departureDate,
            adultCount: searchVM.adultCount,
            teenCount: searchVM.teenCount,
            childrenCount: searchVM.childrenCount,
            tripsByDate: tripsByDate)
        
        return result
    }
}
