//
//  ViewModels.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 04/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

import SwiftUI

struct SearchViewModel {
    
    var originIndex: Int
    var destinationIndex: Int
    var departureDate: Date
    var adultCount: Int
    var teenCount: Int
    var childrenCount: Int
    
    func toModel(stations: [StationModel]) -> SearchModel {
        return SearchModel(
            origin: stations[originIndex],
            destination: stations[destinationIndex],
            departureDate: departureDate,
            adultCount: adultCount,
            teenCount: teenCount,
            childrenCount: childrenCount)
    }
}

class StationsViewModel: ObservableObject {
    
    @Published var stations: [StationViewModel]
    
    init() {
        self.stations = []
    }
    
    init(stations: [StationViewModel]) {
        self.stations = stations
    }
}

struct StationViewModel: Identifiable {
    
    var id: Int
    
    var order: Int
    var name: String
    var codename: String
    
    func toModel() -> StationModel {
        return StationModel(order: order, name: name, codename: codename)
    }
    
    static func fromModel(model: StationModel) -> StationViewModel {
        return StationViewModel(
            id: model.codename.hashValue,
            order: model.order,
            name: model.name,
            codename: model.codename)
    }
}

struct SearchResultViewModel {
    
    var originName: String
    var originCodename: String
    var destinationName: String
    var destinationCodename: String
    
    var departureDate: Date
    
    var adultCount: Int
    var teenCount: Int
    var childrenCount: Int
    
    var tripsByDate: [TripDateViewModel]
    
    init() {
        originName = ""
        originCodename = ""
        destinationName = ""
        destinationCodename = ""
        
        departureDate = Date()
        
        adultCount = 0
        teenCount = 0
        childrenCount = 0
        
        tripsByDate = []
    }
    
    init(originName: String,
         originCodename: String,
         destinationName: String,
         destinationCodename: String,
         departureDate: Date,
         adultCount: Int,
         teenCount: Int,
         childrenCount: Int,
         tripsByDate: [TripDateViewModel]) {
        
        self.originName = originName
        self.originCodename = originCodename
        self.destinationName = destinationName
        self.destinationCodename = destinationCodename
        self.departureDate = departureDate
        self.adultCount = adultCount
        self.teenCount = teenCount
        self.childrenCount = childrenCount
        self.tripsByDate = tripsByDate
    }
}

struct TripDateViewModel: Identifiable {
    
    var id: Int
    
    var date: String
    var flights: [FlightViewModel]
}

struct FlightViewModel: Identifiable {
    
    var id: Int
    
    var time: String
    var flightNumber: String
    var regularFare: String
}
