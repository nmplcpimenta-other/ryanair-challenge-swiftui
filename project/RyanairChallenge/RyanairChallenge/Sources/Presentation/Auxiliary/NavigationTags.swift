//
//  NavigationTags.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 06/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

internal enum NavigationTags {
    
    internal static let tripList: String = "TripList"
}
