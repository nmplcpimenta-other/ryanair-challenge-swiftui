//
//  Strings.swift
//  RyanairChallenge
//
//  Created by Nuno Pimenta on 06/07/2020.
//  Copyright © 2020 Nuno Pimenta. All rights reserved.
//

internal enum Strings {
    
    internal static let alertTitle: String = "Error"
    internal static let alertDismissButton: String = "Dismiss"
    
    internal enum SearchFormView {
        
        internal static let navigationBarTitle: String = "Search"
        
        internal static let sectionStations: String = "Stations"
        internal static let sectionDates: String = "Dates"
        internal static let sectionNumberOfTickets: String = "Number of tickets"
        
        internal static let fieldOrigin: String = "Origin"
        internal static let fieldDestination: String = "Destination"
        internal static let fieldDepartureDate: String = "Departure date"
        internal static let fieldAdults: String = "Adults: "
        internal static let fieldTeens: String = "Teens: "
        internal static let fieldChildren: String = "Children: "
        
        internal static let buttonSearch: String = "Search"
    }
    
    internal enum TripListView {
        
        internal static let noFlightsFound: String = "No flights found"
    }
    
    internal enum TripListSummaryView {
        
        internal static let labelTrip: String = "Trip"
        internal static let labelDate: String = "Date"
        internal static let labelTickets: String = "Tickets"
        
        internal static let ticketCounts: (
            _ adultCount: Int,
            _ teenCount: Int,
            _ childCount: Int) -> String = { (adultCount, teenCount, childCount) in
                
                return
                    "\(adultCount) adult\(adultCount != 1 ? "s" : ""), " +
                    "\(teenCount) teen\(teenCount != 1 ? "s" : ""), " +
                    "\(childCount) \(childCount != 1 ? "children" : "child")"
        }
    }
    
    internal enum SearchBarView {
        
        internal static let fieldPlaceholder: String = "Search"
        internal static let buttonCancel: String = "Cancel"
    }
}
